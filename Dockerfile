ARG OTP_VERSION=latest
FROM registry.gitlab.com/odenwaldnavi/imageschleuder/opentripplanner-latest:${OTP_VERSION} AS otp

# defined empty, so we can access the arg as env later again
ARG OTP_VERSION
ENV ROUTER_NAME=odw

RUN apt-get update && apt-get install -y \
   zip \
   && rm -rf /var/lib/apt/lists/*

ENV RUN="java -Xms15G -Xmx31G -cp /app/resources:/app/classes:/app/libs/* org.opentripplanner.standalone.OTPMain"

ENV OTP_ROOT="/opt/opentripplanner"
WORKDIR ${OTP_ROOT}

RUN mkdir -p ${OTP_ROOT}/build/${ROUTER_NAME}/

# add build data
# NOTE: we're trying to use dockers caching here. add items in order of least to most frequent changes
#ADD odenwald.tiff /opt/opentripplanner/build/$ROUTER_NAME/
#ADD odenwald.gtfs.zip /opt/opentripplanner/build/$ROUTER_NAME/
#ADD 20211210_fahrplaene_gesamtdeutschland_gtfs.zip /opt/opentripplanner/build/$ROUTER_NAME/
#ADD odenwald.pbf /opt/opentripplanner/build/$ROUTER_NAME/
# elevation is spamming the log, leave it out
#RUN ( cd ${OTP_ROOT}/build/${ROUTER_NAME}/ && wget http://192.168.66.250:9000/digitransit-otp-data/odenwald.tiff )
# no NeTEx for now, see https://github.com/opentripplanner/OpenTripPlanner/issues/3640
#RUN ( cd ${OTP_ROOT}/build/${ROUTER_NAME}/ && wget http://192.168.66.250:9000/digitransit-otp-data/20220107_fahrplaene_gesamtdeutschland-vrn.zip )
RUN ( cd ${OTP_ROOT}/build/${ROUTER_NAME}/ && wget http://192.168.66.250:9000/digitransit-otp-data/20230206_fahrplaene_gesamtdeutschland.zip )
RUN ( cd ${OTP_ROOT}/build/${ROUTER_NAME}/ && wget http://192.168.66.250:9000/digitransit-otp-data/odenwald.pbf )
ADD build-config.json ${OTP_ROOT}/build/${ROUTER_NAME}/
ADD otp-config.json ${OTP_ROOT}/build/${ROUTER_NAME}/
ADD router-config.json ${OTP_ROOT}/build/${ROUTER_NAME}/

# print version
RUN ${RUN} --version | tee build/version.txt
RUN echo "image: registry.gitlab.com/odenwaldnavi/opentripplanner:$OTP_VERSION" >> build/version.txt

# build
# -Xmx15G gets use past OSM of Odenwald (new, 275601339 byte version), but gets OOM killed when starting with GTFS
# 15.5 / 16 gets the OOM killer with 16G memory + 4G swap, no idea, just get more memory next time
# now trying with 184143387 byte version and 15G
# build graph from OSM and NeTEx-EU data
RUN /bin/bash -c "set -o pipefail && ${RUN} --build --save build/$ROUTER_NAME | tee build/build.log"
# exit code 137 is the OOM killer

# package: graph and config into zip
RUN cd ${OTP_ROOT}/build && \
    export VERSION=$(${RUN} --version|grep -o -e 'commit: .*,'|grep -o -e '[[:alnum:]]*'|grep -v commit) && \
    if [ -z $VERSION ]; then export VERSION=latest; fi && \
    zip graph-${ROUTER_NAME}-${VERSION}.zip ${ROUTER_NAME}/graph.obj ${ROUTER_NAME}/otp-config.json ${ROUTER_NAME}/router-*.json && \
    zip -r report-${ROUTER_NAME}-${VERSION}.zip ${ROUTER_NAME}/report && \
    rm -rf ${OTP_ROOT}/build/${ROUTER_NAME} && \
    unzip report-${ROUTER_NAME}-${VERSION}.zip && \
    ln -s graph-${ROUTER_NAME}-${VERSION}.zip graph-${ROUTER_NAME}-latest.zip

# ---

FROM nginx:alpine

ENV OTP_ROOT="/opt/opentripplanner"

RUN sed -i 'N; s/index  index.html index.htm;/autoindex on;/' /etc/nginx/conf.d/default.conf; \
    sed -i '/error_page/d' /etc/nginx/conf.d/default.conf
RUN rm /usr/share/nginx/html/*.html

COPY --from=otp ${OTP_ROOT}/build /usr/share/nginx/html/
