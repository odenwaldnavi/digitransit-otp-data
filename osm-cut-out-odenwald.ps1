# soll enthalten den Odenwald
# im Norden  bis Großostheim                         - 49° 55′ N, 9°  5′ O 
# im Osten  bis Hardheim                             - 49° 37′ N, 9° 28′ O
# im Süden  bis Wiesloch                             - 49° 18′ N, 8° 42′ O
# im Westen bis Zwingenberg                          - 49° 43′ N, 8° 37′ O

# und wo Odenwälder hinpendeln
# im Norden bis Frankfurt (Bad Homburg vor der Höhe) - 50° 14′ N, 8° 37′ O
# im Osten bis Wertheim (Village)                    - 49° 46′ N, 9° 35′ O
# im Süden bis Sinsheim/Neckarsulm                   - 49° 12′ N, 9° 13′ O
# im Westen bis Mainz                                - 50°  0′ N, 8° 16′ O

# 2021-12-03T21:21:08Z. File size: 3.5 GB; MD5 sum: 307223863f91165cd08ba927afb65ca1.
# PowerShell 5 wget / Invoke-Webrequest can only handle 2GB Files!!!!!!
# Also appear to be speed handycapped (1/3 to 1/4 of just downloading with Chrome)
#wget https://download.geofabrik.de/europe/germany-211203.osm.pbf


# --complete-ways
./osmconvert64.exe --verbose --statistics "germany-211203.osm.pbf" -b="8.0,49.0,9.5,50.0" -o="odenwald.pbf"
#osmconvert --verbose --statistics "germany-230208.osm.pbf" -b="8.0,49.0,9.5,50.0" -o="odenwald.pbf"
